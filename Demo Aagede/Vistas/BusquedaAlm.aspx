﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="BusquedaAlm.aspx.cs" Inherits="Vistas_BusquedaAlm" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">

<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Agroblockchain</title>
    <link href="../css/menu.css" rel="stylesheet" />
    <link href="../css/site.css" rel="stylesheet" />
    <link href="../css/hover.css" rel="stylesheet" />
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap_css" />
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous" />
    <!-- jquery -->
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" />
    <link rel="stylesheet" href="/resources/demos/style.css" />
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <!-- jquery -->
</head>

<body>
    <form id="form1" runat="server">
        <section class="section_color">
            <a href="../Vistas/Home.aspx" class="navbar-brand2">
                <img src="../images/logo_login.png" width="215" height="150" alt="" />
            </a>
            <div class="container">
                <div id='cssmenu'>
                    <div style="float: right; text-align: right;">
                        <div class="bienvenido">
                            <h4>SISTEMA DE BUSQUEDA Y GESTIÓN DE ALMACENES Y TRANSPORTES </h4>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <%--Contenido--%>
        <div class="container">
            <br />
            <br />
            <div id="pac" class="container espacio_login">
                <div class="row" style="margin-bottom: 25px;"><br/><br/>
                        <div class="col-md-12">
                            <div class="fondo_color6">
                                <img style="float: left;" src="../images/busqueda_.png" width="45" height="48"/>
                                <h3 class="section-title forma_txt">Busquedas de Almacenes</h3>
                            </div>
                        </div>
                </div>        
                <span class="section-divider"></span>

                <div class="row">
                    <div class="row col-md-6">
                        <div class="col-md-12">
                            <asp:TextBox runat="server" Width="1110px" placeholder="Ingresa palabas clave en la busqueda" CssClass="form-control" Style="border-radius: 3px 0px 0px 3px;"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-md-6">
                    </div>
                </div>
                <br />
                <div class="row">
                <div class="reg">
                    <label class="field prepend-icon" style="width: 100%;">
                        <div class="input-group input-group-md" style="padding-bottom: 8px;">
                            Estado
                                <select class="select_cliente">
                                    <option value="Empresa1">Aguascalientes</option>
                                    <option value="Empresa2">Campeche</option>
                                    <option value="Empresa3">Chihuahua</option>
                                    <option value="empresa5">Estado de México</option>
                                    <option value="empresa5">Guanajuato</option>
                                    <option value="empresa5">Guerrero</option>
                                    <option value="empresa5">Tabasco</option>
                                    <option value="empresa5">Veracruz</option>
                                    <option value="empresa5">Zacatecas</option>
                                </select>
                        </div>
                    </label>
                </div>
                <div class="reg2">
                    <label class="field prepend-icon" style="width: 100%;">
                        <div class="input-group input-group-md" style="padding-bottom: 8px;">
                            Tipo de almacen
                                <select class="select_cliente">
                                    <option value="Empresa1">ALMACENAMIENTO CUBIERTO</option>
                                    <option value="Empresa2">ALMACENAMIENTO DESCUBIERTO</option>
                                    <option value="Empresa3">ALMACENAMIENTO DE MATERIAS PRIMAS</option>
                                    <option value="empresa5">ALMACENAMIENTO DE PRODUCTOS INTERMEDIOS</option>
                                    <option value="empresa5">ALMACENAMIENTO DE PRODUCTOS TERMINADOS</option>
                                    <option value="empresa5">ALMACENAMIENTO DE ACCESORIOS</option>
                                    <option value="empresa5">ALMACENAMIENTO DE REFACCIONES</option>
                                </select>
                        </div>
                    </label>
                </div>
                <div class="reg">
                    <label class="field prepend-icon" style="width: 100%;">
                        <div class="input-group input-group-md" style="padding-bottom: 8px;">
                            Municipio
                                <select class="select_cliente">
                                    <option value="Empresa1">Acambay</option>
                                    <option value="Empresa2">Calimaya</option>
                                    <option value="Empresa3">Ecatepec</option>
                                    <option value="empresa5">Jilotepec</option>
                                    <option value="empresa5">Lerma</option>
                                    <option value="empresa5">Metepec</option>
                                    <option value="empresa5">Toluca</option>
                                    <option value="empresa5">Valle de bravo</option>
                                </select>
                        </div>
                    </label>
                </div>
                <div class="reg2">
                    <label class="field prepend-icon" style="width: 100%;">
                        <div class="input-group input-group-md" style="padding-bottom: 8px;">
                            Capacidad
                                <asp:TextBox runat="server" Width="480px" placeholder="Capacidad del almacén" CssClass="form-control" Style="border-radius: 3px 0px 0px 3px;"></asp:TextBox>
                        </div>
                    </label>
                </div>
                <div class="col-md-12">
                    <br />
                    <fieldset>
                        <legend>Servicios adicionales</legend>
                    </fieldset>
                    <div class="col-md-3">
                        <p>
                            <input type="checkbox" />Almacen frigorifico</p>
                    </div>
                    <div class="col-md-3">
                        <p>
                            <input type="checkbox" />SAS (Security Airlock System)</p>
                    </div>
                    <div class="col-md-3">
                        <p>
                            <input type="checkbox" />
                            Precarga en las cámaras de congelación</p>
                    </div>
                    <div class="col-md-3">
                        <p>
                            <input type="checkbox" />
                            Picking automático para preservar la cadena de frio</p>
                    </div>
                </div>
                <div class="col-md-12">
                    <br />
                    <div class="col-md-3">
                        <p>
                            <input type="checkbox" />
                            Camaras de seguridad</p>
                    </div>
                    <div class="col-md-3">
                        <p>
                            <input type="checkbox" />
                            Alarma antirobo</p>
                    </div>
                    <div class="col-md-3">
                        <p>
                            <input type="checkbox" />
                            Espacio para oficina</p>
                    </div>
                    <div class="col-md-3">
                        <p>
                            <input type="checkbox" />
                            Espacio para Maniobras</p>
                        <br />
                        <br />
                    </div>
                </div>
                <div class="col-md-12">
                    <br />
                    <div class="col-md-3">
                        <p>
                            <input type="checkbox" />
                            Servicio de carga y descarga</p>
                    </div>
                    <div class="col-md-3">
                        <p>
                            <input type="checkbox" />
                            Certificado de depósito fiscal</p>
                    </div>
                    <div class="col-md-3">
                        <p>
                            <input type="checkbox" />
                            Personal de vigilancia</p>
                    </div>
                    <div class="col-md-3">
                        <p>
                            <input type="checkbox" />
                            Etiquetado</p>
                        <br />
                        <br />
                    </div>
                </div>
                <%--</div>--%>
                <div class="reg2">
                    <asp:Button ID="btnNuevaGranja" Text="Buscar" runat="server" CssClass="cancelar" OnClick="btnNuevaGranja_Click" />
                </div>
                <div id="div_resultados" visible="false" runat="server" class="col-md-12">
                    <br />
                    <fieldset>
                        <legend>Almacenes localizados   <i class="fas fa-warehouse"></i></legend>
                    </fieldset>
                    <div class="reg">
                        <asp:Image runat="server" Width="600px" Height="655px" OnClick="imgBusquedaAlm_Click" ImageUrl="../images/busqueda_alm.png" />
                    </div>
                    <div class="reg2">
                        <div class="content fadeIn animated" style="animation-duration: 1s;">
                            <div class="col-md-12">
                                <br />
                                <div class="form-horizontal">
                                    <fieldset>
                                        <div id="accordion">
                                            <!-- <a data-toggle="collapse" href="#faq3" class="collapsed">Asignación de granjas <i class="fa fa-plus-circle der"></i></a> -->
                                            <h3 class="collapsed">AAACESA Almacenes Fiscalizados    <i class="fas fa-warehouse"></i></h3>
                                            <div>
                                                <div class="col-md-12">
                                                    <br />
                                                    <p style="font-family: Verdana, Geneva, Tahoma, sans-serif; font-weight: bold">Descripción:</p>
                                                    <asp:Label runat="server" Text="Almacén de refrigerados y congelados con equipo de última generación para el manejo de sus productos. Cámaras divididas para una mejor ubicación de posiciones y mayor control de inventario. Temperaturas de 4° a ‐25°. Sistema de seguridad en todas las cámaras en tiempo real. Espacio para oficinas. Certificación TIF."></asp:Label>
                                                    <br />
                                                    <br />
                                                    <p style="font-family: Verdana, Geneva, Tahoma, sans-serif; font-weight: bold">Capacidad:</p>
                                                    <asp:Label runat="server" Text="10,000 m2 de superficie"></asp:Label>
                                                    <br />
                                                    <br />
                                                    <p style="font-family: Verdana, Geneva, Tahoma, sans-serif; font-weight: bold">Telefono:</p>
                                                    <asp:Label runat="server" Text="5555-555555-555555"></asp:Label>
                                                    <br />
                                                    <br />
                                                    <p style="font-family: Verdana, Geneva, Tahoma, sans-serif; font-weight: bold">Web:</p>
                                                    <asp:Label runat="server" Text="www.almacenesfiscalizadosaaacesa.com"></asp:Label>
                                                </div>
                                            </div>
                                            <h3>Servicios adicionales  <i class="fab fa-servicestack"></i></h3>
                                            <div>
                                                <br />
                                                <br />
                                                <div class="col-md-12">
                                                    <br />
                                                    <div class="col-md-4">
                                                        <p>
                                                            <input type="checkbox" />
                                                            Almacen frigorifico</p>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <p>
                                                            <input type="checkbox" />
                                                            SAS (Security Airlock System)</p>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <p>
                                                            <input type="checkbox" />
                                                            Precarga en las cámaras de congelación</p>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <br />
                                                    <div class="col-md-4">
                                                        <p>
                                                            <input type="checkbox" />
                                                            Camaras de seguridad</p>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <p>
                                                            <input type="checkbox" />
                                                            Alarma antirobo</p>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <p>
                                                            <input type="checkbox" />
                                                            Espacio para oficina</p>
                                                        <br />
                                                        <br />
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <br />
                                                    <div class="col-md-4">
                                                        <p>
                                                            <input type="checkbox" />
                                                            Certificado de depósito fiscal</p>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <p>
                                                            <input type="checkbox" />
                                                            Picking automático para preservar la cadena de frío</p>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <p>
                                                            <input type="checkbox" />
                                                            Espacio para maniobras</p>
                                                        <br />
                                                        <br />
                                                    </div>
                                                </div>
                                            </div>
                                            <h3>Calificaciones  <i class="far fa-star"></i></h3>
                                            <div>
                                                <br />
                                                <div class="col-md-12">
                                                    <h4>Agroservicios Nieto SA DE CV</h4>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="col-md-6">
                                                        <asp:Image runat="server" ImageUrl="../images/5est.png" />
                                                    </div>
                                                    <div class="col-md-6">
                                                        Excelente servicio de almacenaje
                                                    </div>
                                                </div>
                                                <br />
                                                <div class="col-md-12">
                                                    <h4>Agroindustrias Unidas de México SA DE CV</h4>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="col-md-6">
                                                        <asp:Image runat="server" ImageUrl="../images/5est.png" />
                                                    </div>
                                                    <div class="col-md-6">
                                                        Almacenadora ampliamente recomendada
                                                    </div>
                                                </div>
                                                <br />
                                                <div class="col-md-12">
                                                    <h4>Meagromex SA de CV</h4>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="col-md-6">
                                                        <asp:Image runat="server" ImageUrl="../images/3est.png" />
                                                    </div>
                                                    <div class="col-md-6">
                                                        Se presentó una falla en una cámara de refrigeración, pero se resolvió el problema. 
                                                    </div>
                                                </div>
                                            </div>
                                            <h3>Cotizar  <i class="fab fa-servicestack"></i></h3>
                                            <div>
                                                <div class="col-md-12">
                                                    Seleccione el periodo para la renta del servicio:
                                                </div>
                                                <br />
                                                <br />
                                                <div class="reg">
                                                    Inicio:
                                                          <asp:TextBox runat="server" placeholder="DD/MM/YYYY" CssClass="form-control" Style="border-radius: 3px 0px 0px 3px;" Text="10/03/2019"></asp:TextBox>
                                                </div>
                                                <div class="reg2">
                                                    Fin:
                                                         <asp:TextBox runat="server" placeholder="DD/MM/YYYY" CssClass="form-control" Style="border-radius: 3px 0px 0px 3px;" Text="12/04/2019"></asp:TextBox>
                                                </div>

                                                <div class="col-md-12">
                                                    <br />
                                                    <input class="verifica" type="button" value="Verificar disponibilidad" />
                                                </div>
                                                <div style="font-weight: bold; font-size: 23px; color: darkgreen" class="col-md-12">
                                                    <br />
                                                    Disponible
                                                </div>
                                                <div style="font-weight: bold;" class="col-md-12">
                                                    <br />
                                                    Total por periodo y servicios seleccionados: 
                                                </div>
                                                <div class="col-md-12">
                                                    <br />
                                                    <div class="col-md-6"> 
                                                    </div>
                                                    <div class="col-md-6">
                                                         <br />
                                                        <div style="font-weight: bold; font-size: 23px; color: darkblue">
                                                             <img src="../images/coinFondoBlanco.jpg" width="27" height="27" /> 18.00 XCO
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </fieldset>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="col-md-6"></div>
                                <div class="col-md-6">
                                    <br />
                                    <br />
                                    <asp:Button ID="AgregarCompra" runat="server" Text="Agregar a la compra" CssClass="salvar" OnClick="AgregarCompra_Click" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
       </div>
    </form>
    <br />
    <br />
    <footer id="myFooter"><br />
        <div class="footer-copyright">
            <img src="../images/logo_bnco.png" width="160" height="36" alt="" />
            <p style="color: black">Bloxity development 2019</p>
        </div>
    </footer>
</body>
<script>
    $(function () {
        $("#accordion").accordion();

    });
</script>
<style>
    #accordion
    .ui-accordion-content {
        width: 100%;
        color: rgb(43, 39, 39);
        font-size: 10pt;
        line-height: 16pt;
    }
</style>
</html>
