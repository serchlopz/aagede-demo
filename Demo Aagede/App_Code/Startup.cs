﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(DemoCNA.Startup))]
namespace DemoCNA
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
